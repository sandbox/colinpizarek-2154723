<?php
/**
 * @file
 * A block module that retrieves and displays upcoming events from a NeonCRM database.
 */
 
/**
 * Implements hook_permission().
 */
function neon_event_list_permission() {
  return array(
    'view neon event list' => array(
      'title' => t('View event list'),
      'description' => t('View the content block that contains the NeonCRM Event List'),
      ),
    );
}
/**
 * Implements hook_enable().
 */
function neon_event_list_enable() {
  $permissions = array('view neon event list' => TRUE);
  user_role_change_permissions(DRUPAL_ANONYMOUS_RID, $permissions);
  user_role_change_permissions(DRUPAL_AUTHENTICATED_RID, $permissions);
}
/**
 * Implements hook_block_info().
 */
function neon_event_list_block_info() {
  $blocks['neon_event_list'] = array(
    'info' => t('Upcoming Events from NeonCRM'),
    'cache' => DRUPAL_CACHE_PER_ROLE,
  );
  return $blocks;
}
/**
 * Implements hook_help();
 */
function neon_event_list_help($path, $arg) {
  if ($path == 'admin/help#neon_event_list') {
    return t('Configure NeonCRM Event List');
  }
}
/**
 * Implements hook_menu().
 */
function neon_event_list_menu() {
  $items = array();
  $items['admin/config/neon_event_list/settings'] = array(
    'title' => 'Settings',
    'description' => 'Configure your event list settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('neon_event_list_form'),
    'access arguments' => array('administer modules'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}
/**
 * Implements hook_form().
 * 
 * Configuration page for settings.
 */
function neon_event_list_form($form, &$form_state) {
// Credentials
  $form['credentials'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Credentials'), 
    '#description' => t('Enter your NeonCRM API Key. You can find more information about obtaining an API key or Organization ID here: <a target="_blank" href="https://z2.zendesk.com/entries/21818899">Obtaining an API Key</a>'),
    '#collapsible' => FALSE, 
  );
 // API Key
  $form['credentials']['neon_event_list_api_key'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('neon_event_list_api_key', NULL),
    '#title' => 'API Key',
    '#size' => '20',
    '#maxlength' => '120',
    '#required' => TRUE,
  );
// Organization ID
  $form['credentials']['neon_event_list_org_id'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('neon_event_list_org_id', NULL),
    '#title' => 'Organization ID',
    '#size' => '10',
    '#maxlength' => '20',
    '#required' => TRUE,
  );
// Search Filters Section
  $form['search_filters'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Search Filters'), 
    '#description' => t('Configure the search parameters when retrieving events.'),
    '#collapsible' => FALSE, 
  );
// Number of Events to Display
  $form['search_filters']['neon_event_list_display_number'] = array(
    '#type' => 'select',
    '#default_value' => variable_get('neon_event_list_display_number', 5),
    '#title' => 'Number of Events to Display',
    '#options' => array(1, 3, 5, 10, 25),
    '#description' => t('Maximum number of events to display'),
  );
// Category
  $form['search_filters']['neon_event_list_category'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('neon_event_list_category', NULL),
    '#title' => 'Event Category',
    '#size' => '20',
    '#maxlength' => '120',
    '#required' => FALSE,
    '#description' => t('Limit your results to a single event category.'),
  );
// Output Columns
  $form['columns'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Output Columns'), 
    '#description' => t('Configure which data is displayed in your event list.'),
    '#collapsible' => FALSE, 
  );
// Event Name
  $form['columns']['neon_event_list_event_name'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('neon_event_list_event_name', TRUE),
    '#title' => 'Event Name',
    '#required' => FALSE,
  );
// Start Date
  $form['columns']['neon_event_list_event_start_date'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('neon_event_list_event_start_date', TRUE),
    '#title' => 'Event Start Date',
    '#required' => FALSE,
  );
// Start Time
  $form['columns']['neon_event_list_event_start_time'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('neon_event_list_event_start_time', FALSE),
    '#title' => 'Event Start Time',
    '#required' => FALSE,
  );
// Event Detail Link
  $form['columns']['neon_event_list_event_detail'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('neon_event_list_event_detail', TRUE),
    '#title' => 'Event Detail Link',
    '#required' => FALSE,
  );
// Event Registration Link
  $form['columns']['neon_event_list_event_reg'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('neon_event_list_event_reg', TRUE),
    '#title' => 'Event Registration Link',
    '#required' => FALSE,
  );

  return system_settings_form($form);
}

/**
 * Implements hook_block_view().
 *
 * Prepares the contents of the block.
 */
function neon_event_list_block_view($delta = '') {
  if (!user_access('view neon event list')) {
    return;
  }
  $block['subject'] = t('Upcoming Events');
  $block['content'] = NULL;
  /**
   * Logs in to the NeonCRM API.
   *
   * Retrieves the Organization ID and API Key from settings,
   * then attempts to obtain a session ID from NeonCRM using the Login method.
   * If successful, the session ID is extracted and saved as a session variable.
   */
  function login() {
    $org_id = t(variable_get('neon_event_list_org_id', NULL));
    $api_key = t(variable_get('neon_event_list_api_key', NULL));
    // Check for Org ID & API key. If not there, return
    if (isset($org_id) && isset($api_key)) {
      $data = array(
        'login.apiKey' => $api_key,
        'login.orgid' => $org_id,
      );
      $options = array(
        'method' => 'POST',
        'data' => drupal_http_build_query($data),
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      );
      $login = drupal_http_request('https://api.neoncrm.com/neonws/services/api/common/login', $options); // Logs in to NeonCRM API and saves result as variable
      $login = json_decode($login->data, TRUE); // debugging
      if ($login['loginResponse']['operationResult'] == 'SUCCESS') { // Check for successful login
        $_SESSION['neon_session_id'] = $login['loginResponse']['userSessionId']; // Set session variable
        echo '<pre>Logged in successfully</pre>'; // Debugging message
      } 
      else {
        return 'Login failed<br>' . var_dump($login); // Debugging message
      }
    } 
    else {
      return 'Organization ID or API key is missing'; // Debugging message
    }
  }
  /**
   * Retreives events from the NeonCRM API.
   *
   * Builds a query from the configuration settings, then submits the query
   * to NeonCRM using the listEvents method.
   * If successful, the event data is saved as an associated array.
   */
  function get_neon_events() {
    if (isset($_SESSION['neon_session_id'])) {
/* // Begin API Dependent Section
      $neon_session_id = $_SESSION['neon_session_id'];
      $data = array(
        'userSessionId' => $neon_session_id,
        'outputfields.idnamepair.name' => 'Event Name',
        'Page.pageSize' => t(variable_get('neon_event_list_display_number', 5)),
        'Page.sortColumn' => 'Event Start Date',
        'Page.sortDirection' => 'ASC',
      );
      //Add event category search if available
      $event_category = variable_get('neon_event_list_category', NULL);
      if (isset($event_category)) {
        $data['searches.search.key'] = 'Event Category Name';
        $data['searches.search.searchOperator'] = 'EQUAL';
        $data['searches.search.value'] = t(variable_get('neon_event_list_category'));
      } 
      else {
      }
      //prepare the request
      $options = array(
        'method' => 'POST',
        'data' => drupal_http_build_query($data),
        'timeout' => 15,
        'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
      );
      $events = drupal_http_request('https://api.neoncrm.com/neonws/services/api/event/listEvents', $options); // Requests events from API and saves result as variable
      var_dump($events);
*/ // End API Dependent Section
      $events = '{"listEvents":{"operationResult":"SUCCESS","responseDateTime":"2013-02-24T21:21:48.952-06:00","page":{"currentPage":1,"pageSize":10,"totalPage":2,"totalResults":13},"searchResults":{"nameValuePairs":[{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":"  Ralf event(Copied)(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":649},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":624},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":634},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":614},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":629},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":639},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"01:00:00"},{"name":"Campaign Name","value":"Annual Campaign"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-22"},{"name":"Event Name","value":" Ralf event(Copied)"},{"name":"Event Start Date","value":"2012-08-20"},{"name":"Campaign Id","value":4},{"name":"Event Id","value":609},{"name":"Event End Time","value":"05:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"19:00:00"},{"name":"Campaign Name","value":"$5 Support a Kid"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-16"},{"name":"Event Name","value":"jessee hold a party"},{"name":"Event Start Date","value":"2012-08-15"},{"name":"Campaign Id","value":249},{"name":"Event Id","value":669},{"name":"Event End Time","value":"22:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"19:00:00"},{"name":"Campaign Name","value":"$5 Support a Kid"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-30"},{"name":"Event Name","value":"jessee hold a party"},{"name":"Event Start Date","value":"2012-08-29"},{"name":"Campaign Id","value":249},{"name":"Event Id","value":679},{"name":"Event End Time","value":"22:00:00"},{"name":"Registration Attendee Count","value":0}]},{"nameValuePair":[{"name":"Event Start Time","value":"19:00:00"},{"name":"Campaign Name","value":"$5 Support a Kid"},{"name":"Total Revenue"},{"name":"Event End Date","value":"2012-08-09"},{"name":"Event Name","value":"jessee hold a party"},{"name":"Event Start Date","value":"2012-08-08"},{"name":"Campaign Id","value":249},{"name":"Event Id","value":664},{"name":"Event End Time","value":"22:00:00"},{"name":"Registration Attendee Count","value":0}]}]}}}'; // This is test JSON data
      $events = json_decode($events, TRUE);
      /*$events = json_decode($events->data); // extracts json from response and turns it into an array*/
      if ($events['listEvents']['operationResult'] == 'SUCCESS' && $events['listEvents']['page']['totalResults'] > 0) {
        return $events; // return the API request as an array
      } 
      else {
        echo 'Request failed<br>'; // Debugging message
        return;
      }
    } 
    else {
      return 'No session present'; // Debugging message
      return;
    }
  }
  /**
   * Converts retrieved event data into a better format.
   *
   * Converts API data into a better-formed associative array.
   * Checks settings to see if specific data is supposed to be rendered.
   * Passes data array to drupal theme function.
   */
  function format_neon_events($events) {
    $neon = array();
    foreach ($events['listEvents']['searchResults']['nameValuePairs'] as $key => $event) {
      foreach ($event['nameValuePair'] as $field) {
        $name = $field['name'];
        if (isset($field['value'])) {
          $valu = $field['value'];
        } 
        else {
          $valu = NULL;
        }
        $neon[$key][$name] = $valu;
      }
    }
    return $neon;
  }
  if (isset($_SESSION['neon_session_id'])) {
    $login = login(); // Debugging Function
    $events = get_neon_events();
    $formatted_event = format_neon_events($events);
  } 
  else {
    $login = login();
    $events = get_neon_events();
    $formatted_event = format_neon_events($events);
  }
  foreach ($formatted_event as $event) {
    $fields = array();
    // Check for event name
    if (variable_get('neon_event_list_event_name')) {
      $neon_event_name = t($event['Event Name']);
    } 
    else {
      $neon_event_name = NULL;
      //echo '<pre>no name</pre>'; // Debugging Message
    }
    // Check for start date
    if (variable_get('neon_event_list_event_start_date')) {
      $fields[] = format_date(strtotime($event['Event Start Date']), $type = 'long');
    } 
    else {
      //echo '<pre>no start date</pre>'; // Debugging Message
    }
    // Check for start time
    if (variable_get('neon_event_list_event_start_time')) {
      $fields[] = format_date(strtotime($event['Event Start Time']), $type = 'short');
    }
    else {
      //echo '<pre>no start time</pre>'; // Debugging Message
    }
    // Check for detail link
    if (variable_get('neon_event_list_event_detail')) {
      $fields[] = t('<a class="neon_event_detail" href="https://trial.z2systems.com/np/clients/test/event.jsp?event=' . $event['Event Id'] . '">More Information</a>');
    } 
    else {
      //echo '<pre>no detail</pre>'; // Debugging Message
    }
    // Check for registration link
    if (variable_get('neon_event_list_event_reg')) {
      $fields[] = t('<a class="neon_event_registration" href="https://trial.z2systems.com/np/clients/test/eventRegistration.jsp?event=' . $event['Event Id'] . '">Register</a>');
    } 
    else {
      //echo '<pre>no registration</pre>'; // Debugging Message
    }
    $attributes['class'] = 'neon_event_' . $event['Event Id'];
    $block['content'] .= theme('item_list', array('items' => $fields, 'title' => $neon_event_name, 'attributes' => $attributes));
    }
    return $block;
  }